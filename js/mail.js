$(document).ready(function() {
    if ($("#firstname").attr('aria-invalid')) {
        $("#firstname-field").css('background-color', 'red')
    }
    $("#signupForm").validate({
        onfocusout: function(element) {
            this.element(element)
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            firstname: {
                required: true,
            },

            lastname: {
                required: true,
            },
            zipcode: {
                required: true,
            },
            item: {
                required: true,
            },
            note: {
                required: true,
            },
            agree: {
                required: true,
            }
        },
        messages: {
            email: {
                required: '未入力チェック、形式チェック（メールアドレス）',
                email: 'エラーメール',
            },
            firstname: {
                required: "未入力チェック",
            },
            lastname: {
                required: "未入力チェック",
            },
            zipcode: {
                required: "形式チェック（数値）",

            },
            item: {
                required: "未入力チェック",
            },
            note: {
                required: "未入力チェック",
            },
            agree: {
                required: "未入力チェック",
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "item") {
                error.insertAfter("#mg-item");
            } else if (element.attr("name") == "agree") {
                error.insertAfter("#mg-agree");
            } else {
                error.insertAfter(element);
            }
        }
    });
});

function sendEmail() {
    var name = $("#name");
    var email = $("#email");
    var subject = $("#subject");
    var body = $("#body");

    if (isNotEmpty(name) && isNotEmpty(email) && isNotEmpty(subject) && isNotEmpty(body)) {
        $.ajax({
            url: 'database.php',
            method: 'POST',
            dataType: 'json',
            data: {
                name: name.val(),
                email: email.val(),
                subject: subject.val(),
                body: body.val()
            },
            success: function(response) {
                if (response.status == "success")
                    alert('Email Has Been Sent!');
                else {
                    alert('Please Try Again!');
                    console.log(response);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
}