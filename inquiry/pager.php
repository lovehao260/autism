<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>山梨県自閉症協会 - 一人で悩んでいないで私たちとお話ししてみませんか</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="../css/ress.css">
  <link rel="stylesheet" href="../css/style.css">
</head>
<body>
  <header>
    <div id="header-inner">
      <div class="logo">
        <a href="../index.html">
          <img src="../images/logo.png" alt="一般社団法人日本自閉症協会 団体加盟会員">
        </a>
      </div>
      <nav>
        <ul>
          <li>
            <a href="../index.html">トップページ</a>
          </li>
          <li>
            <a href="../about.html">⾃閉症協会について</a>
          </li>
          <li>
            <a href="../membership.html">⼊会のご案内</a>
          </li>
          <li>
            <a href="../autism.html">⾃閉症ってなあに︖</a>
          </li>
          <li>
            <a href="../list.html">各種機関窓⼝⼀覧</a>
          </li>
          <li class="menu-a active">
            <a href="pager.php">お問い合わせ</a>
          </li>
        </ul>
      </nav>
      <div class="banner">
        <div class="banner-title">
          <p>自閉症などの広汎性発達障害のお子さんをお持ちのお父さん・お母さん、<br>一人で悩んでいないで<br>私たちといっしょにお話ししてみませんか？</p>
        </div>
      </div>
    </div>
  </header>
  <article class="content-page">
    <div class="container">
      <div class="column-left-list">
        <div class="breadcrumbs">
          <a href="index.html">トップページ</a> >
          <a href="pager.php">From-mail</a>
        </div>
        <h1>お問い合わせ</h1>
        <div class="pico_body">
          <p>山梨県自閉症協会に入会をご希望の方は、入会申込書をお送りいたしますので、必ず郵便番号、住所をご記入ください。入会手続きについて、詳しくは<a href="../membership.html">山梨県自閉症協会入会のご案内</a>をご覧ください。</p>
          <form class="cmxform" id="signupForm" method="post" action="#">
            <dl class="mailform">
              <dt>
                お名前<span class="red">（必須）</span>
              </dt>
              <dd>記入例）　山梨　花子<br>
                <input id="firstname" name="firstname" type="text" size="50">
              </dd>
              <dt>
                お名前ふりがな<span class="red">（必須）</span>
              </dt>
              <dd>記入例）　やまなし　はなこ<br>
                <input id="lastname" name="lastname" type="text" size="50">
              </dd>
              <dt>
                メールアドレス<span class="red">（必須）</span>
              </dt>
              <dd>記入例）　info@asj-yamanashi.org<br>
                <input id="email" name="email" type="text" size="50">
              </dd>
              <dt>
                郵便番号
              </dt>
              <dd>記入例）　400-0000<br>
                <input id="zipcode" name="zipcode" type="text" size="30">
              </dd>
              <dt>
                ご住所
              </dt>
              <dd>記入例）　山梨県甲府市丸の内0-00-00<br>
                <input id="add" name="add" type="text" size="50">
              </dd>
              <dt>
                性別
              </dt>
              <dd>
                <label for="sex1">
                  <input type="radio" id="sex1" value="0" name="sex">男
                </label>
                <label for="sex2">
                  <input type="radio" id="sex2" value="1" name="sex">女
                </label>
              </dd>
              <dt>
                年代
              </dt>
              <dd>
                <label for="generation1">
                  <input type="radio" id="generation1" value="0" name="generation">20歳以下
                </label>
                <label for="generation2">
                  <input type="radio" id="generation2" value="1" name="generation">20歳代
                </label>
                <label for="generation3">
                  <input type="radio" id="generation3" value="2" name="generation">30歳代
                </label>
                <label for="generation4">
                  <input type="radio" id="generation4" value="3" name="generation">40歳代
                </label>
                <label for="generation5">
                  <input type="radio" id="generation5" value="4" name="generation">50歳代  
                </label>
                <label for="generation6">
                  <input type="radio" id="generation6" value="5" name="generation">60歳以上
                </label>
              </dd>
              <dt>
                あなたと自閉症との関係は？
              </dt>
              <dd>
                <label for="relation1">
                  <input type="radio" id="relation1" value="0" name="relation">当事者
                </label>
                <label for="relation2">
                  <input type="radio" id="relation2" value="1" name="relation">保護者
                </label>
                <label for="relation3">
                  <input type="radio" id="relation3" value="3" name="relation">支援者
                </label>
              </dd>
              <dt>
                お問い合わせ項目
              </dt>
              <dd>
                <label for="item1">
                  <input type="checkbox" id="item1" value="0" name="item">自閉症協会入会について
                </label><br>
                <label for="item2">
                  <input type="checkbox" id="item2" value="1" name="item">活動内容にについて
                </label><br>
                <label for="item3">
                  <input type="checkbox" id="item3" value="2" name="item">パスワードについて（会員）
                </label><br>
                <label for="item4">
                  <input type="checkbox" id="item4" value="3" name="item">その他
                </label>
                <div id="mg-item"></div>
              </dd>
              <dt>
                お問い合わせ内容<span class="red">(必須)</span>
              </dt>
              <dd>
                <textarea cols="45" rows="10" name="note"></textarea>
              </dd>
              <dt>
                個人情報の取扱い
              </dt>
              <dd>
                <textarea name="privacy" cols="45" rows="8" readonly="readonly" accesskey="p" tabindex="22">・山梨県自閉症協会は、このお問い合わせフォームでご提供いただきました個人情報を、お問い合わせへの回答の目的のために利用いたします。

・ご提供いただきました個人情報が流出しないようその管理に最大限努力し、本人の許可なく個人情報を第三者に提供することはありません。ただし、法律の適用を受ける場合や法的効力のある要求による場合には、この限りではありません。またそれらの情報を営利目的のために使用することもございません。

・本フォームでご提供頂きましたメールアドレスに誤りがあった場合やシステム障害などの場合にはご回答できない場合があります。

・お問い合わせの内容によっては、電話や書面にて回答させていただく場合があります。</textarea><br>
                <label for="agree">
                  <input type="checkbox" class="checkbox" id="agree" value="0" name="agree"> 上記事項に同意します
                </label>
                <div id="mg-agree"></div>
              </dd>
            </dl>
            <input class="submit" type="submit" value="内容確認ページへ">
            <input class="reset" type="reset" value="リセット">
          </form>
        </div>
      </div>
    </div>
  </article>
  <footer>
    <div class="footerinfo">
      <p>Copyright © 2009 ⼭梨県⾃閉症協会 All Rights Reserved.</p>
    </div>
  </footer>

    
  <!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery.validate.min.js"></script>
  <script src="../js/mail.js"></script>
    
</body>
</html>