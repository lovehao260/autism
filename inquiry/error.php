<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Contact Form</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="../css/ress.css">
  <link rel="stylesheet" href="../css/style.css">
</head>
<body>
  <header>
    <div id="header-inner">
      <div class="logo">
        <a href="../index.html">
          <img src="../images/logo.png" alt="一般社団法人日本自閉症協会 団体加盟会員">
        </a>
      </div>
      <div class="menu">
        <ul class="title-menu">
          <li class="menu-a active">
            <a href="../index.html">トップページ</a>
          </li>
          <li>
            <a href="../about.html">⾃閉症協会について</a>
          </li>
          <li>
            <a href="../membership.html">⼊会のご案内</a>
          </li>
          <li>
            <a href="../autism.html">⾃閉症ってなあに︖</a>
          </li>
          <li>
            <a href="../list.html">各種機関窓⼝⼀覧</a>
          </li>
          <li>
            <a href="pager.php">お問い合わせ</a>
          </li>
        </ul>
      </div>
      <div class="banner">
        <div class="banner-title">
          <p>自閉症などの広汎性発達障害のお子さんをお持ちのお父さん・お母さん、<br>一人で悩んでいないで<br>私たちといっしょにお話ししてみませんか？</p>
        </div>
      </div>
    </div>
  </header>
  <article class="content-subpage">
    <div class="container">
      <div class="column-left-list">
        <h1>山梨県自閉症協会　お問い合わせの確認</h1>
        <p class="error-text">入力に不備があります！</p>
        <p class="mailerror">お名前（必須）が入力されていません。<br>お名前ふりがな（必須）が入力されていません。<br> メールアドレス（必須）が入力されていません。<br>お問い合わせ内容（必須）が入力されていません。<br>個人情報の取扱いが入力されていません。</p>
        <p class="mailresult">郵便番号：<br>ご住所：</p><br>
        <form name="form1" method="post" action="page.php">
          <p class="error-text">入力に不備あります。戻るボタンで前の画面へお戻りください。</p>
          <input type="submit" value="戻る">
        </form>
      </div>
    </div>
  </article>
  <footer>
    <div class="footerinfo">
      <p>Copyright © 2009 ⼭梨県⾃閉症協会 All Rights Reserved.</p>
    </div>
  </footer>

  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="../js/jquery.min.js"></script>
  <script src="../js/jquery.validate.min.js"></script>
  <script>
    $(document).ready(function() {
    function sendEmail() {
      var name = $("#name");
      var email = $("#email");
      var subject = $("#subject");
      var body = $("#body");

      if (isNotEmpty(name) && isNotEmpty(email) && isNotEmpty(subject) && isNotEmpty(body)) {
        $.ajax({
          url: 'database.php',
          method: 'POST',
          dataType: 'json',
          data: {
            name: name.val(),
            email: email.val(),
            subject: subject.val(),
            body: body.val()
          }, 
          success: function (response) {
              if (response.status == "success")
                alert('Email Has Been Sent!');
              else {
                alert('Please Try Again!');
                console.log(response);
              }
          }, 
          error: function(xhr, ajaxOptions, thrownError){
            alert(thrownError);
          }
        });
      }
    }

    function isNotEmpty(caller) {
      if (caller.val() == "") {
        caller.css('border', '1px solid red');
        return false;
      } else
        caller.css('border', '');
      return true;
    }
  </script>
  
</body>
</html>