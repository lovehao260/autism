<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>山梨県自閉症協会 - 一人で悩んでいないで私たちとお話ししてみませんか</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="../css/ress.css">
  <link rel="stylesheet" href="../css/style.css">
</head>
<body>
  <header>
    <div id="header-inner">
      <div class="logo">
        <a href="../index.html">
          <img src="../images/logo.png" alt="一般社団法人日本自閉症協会 団体加盟会員">
        </a>
      </div>
      <div class="menu">
        <ul class="title-menu">
          <li class="menu-a active">
            <a href="../index.html">トップページ</a>
          </li>
          <li>
            <a href="../about.html">⾃閉症協会について</a>
          </li>
          <li>
            <a href="../membership.html">⼊会のご案内</a>
          </li>
          <li>
            <a href="../autism.html">⾃閉症ってなあに︖</a>
          </li>
          <li>
            <a href="../list.html">各種機関窓⼝⼀覧</a>
          </li>
          <li>
            <a href="pager.php">お問い合わせ</a>
          </li>
        </ul>
      </div>
      <div class="banner">
        <div class="banner-title">
          <p>自閉症などの広汎性発達障害のお子さんをお持ちのお父さん・お母さん、<br>一人で悩んでいないで<br>私たちといっしょにお話ししてみませんか？</p>
        </div>
      </div>
    </div>
  </header>
  <article class="content-subpage">
    <div class="container">
      <div class="column-left-list">
        <h1>山梨県自閉症協会　お問い合わせの確認</h1>
        <div class="content-mail">
        </div>
        <p class="checkmail">この内容でよろしければ送信ボタンを押して送信ください。<br>
            やり直す場合は、戻るボタンで前の画面へお戻りください。
        </p>
        <label for="agree">
            <input type="checkbox" class="checkbox-agree" id="agree" value="0" name="agree" aria-invalid="true"> 自動返信を受取らない
        </label>
        <input class="submit" type="submit" value="送信">
        <input class="reset" type="reset" value="戻る">
      </div>
    </div>
  </article>
  <footer>
    <div class="footerinfo">
      <p>Copyright © 2009 ⼭梨県⾃閉症協会 All Rights Reserved.</p>
    </div>
  </footer>
  
</body>
</html>